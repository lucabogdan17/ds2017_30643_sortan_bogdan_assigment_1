package servlets;

import controller.HourCalculator;
import controller.UserController;
import model.Flight;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/user")
public class UserServlet extends HttpServlet {
    private UserController userController=new UserController();
    private HourCalculator hourCalculator=new HourCalculator();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Flight> flights = userController.getAllFlights();
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();


        if (request.getParameter("flights") != null) {
            out.print("<table>");
            out.print("<form method=" + "post" + ">");
            out.print("<table style=" + "float:left;" + ">");
            out.print("<tr>");
            out.print("<th>" + "Flight number" + "</th>");
            out.print("<th>" + "Airplane type" + "</th>");
            out.print("<th>" + "Departure city " + "</th>");
            out.print("<th>" + "Departure date " + "</th>");
            out.print("<th>" + "Arrival city" + "</th>");
            out.print("<th>" + "Arrival date" + "</th>");
            out.print("</tr>");
            for (Flight flight : flights) {
                out.print("<tr>");
                out.print("<td>" + flight.getFlightnumber() + "</td>");
                out.print("<td>" + flight.getAirplanetype() + "</td>");
                out.print("<td>" + userController.getCityById(flight.getDepcity()).getName() + "</td>");
                out.print("<td>" + flight.getDepdate() + "</td>");
                out.print("<td>" + userController.getCityById(flight.getArvcity()).getName() + "</td>");
                out.print("<td>" + flight.getArvdate() + "</td>");
                out.print("</tr>");
            }
            out.print("</table>");
        } else if ((request.getParameter("flightnr") != null)) {
            if (request.getParameter("flightnr").equals("")) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Please enter flight number');");
                out.println("location='userPannel.html';");
                out.println("</script>");
            } else {
                if (userController.getFlightByNumber(Integer.parseInt(request.getParameter("flightnr"))) != null) {
                    Flight currentFlight=userController.getFlightByNumber(Integer.parseInt(request.getParameter("flightnr")));
//                    hourCalculator.calculateHour(userController.getCityById(currentFlight.getDepcity()));
//                    hourCalculator.calculateHour(userController.getCityById(currentFlight.getArvcity()));
                    out.print("<table>");
                    out.print("<form method=" + "post" + ">");
                    out.print("<table style=" + "float:left;" + ">");
                    out.print("<tr>");
                    out.print("<th>" + "City" + "</th>");
                    out.print("<th>" + "DateTime" + "</th>");
                    out.print("</tr>");
                    out.print("<tr>");
                    out.print("<td>" + userController.getCityById(currentFlight.getDepcity()).getName() + "</td>");
                    out.print("<td>" + hourCalculator.calculateHour(userController.getCityById(currentFlight.getDepcity())) + "</td>");
                    out.print("</tr>");
                    out.print("<tr>");
                    out.print("<td>" + userController.getCityById(currentFlight.getArvcity()).getName() + "</td>");
                    out.print("<td>" + hourCalculator.calculateHour(userController.getCityById(currentFlight.getArvcity())) + "</td>");
                    out.print("</tr>");
                    out.print("</table>");

                } else {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Flight inexistent');");
                    out.println("location='userPannel.html';");
                    out.println("</script>");
                }
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

    }
}

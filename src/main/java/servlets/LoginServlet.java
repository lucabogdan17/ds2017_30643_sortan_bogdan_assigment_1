package servlets;

import controller.LoginController;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
       /* response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print("<table></table>")
        */
        request.getRequestDispatcher("login.html").forward(request, response);
    }
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LoginController loginController=new LoginController();
        PrintWriter out = response.getWriter();

        String n=request.getParameter("username");
        String p=request.getParameter("password");
        User user=new User();
        user.setUsername(n);
        user.setPassword(p);
        User currentUser=loginController.validate(user);
        if(currentUser!=null){

            if(currentUser.getType().equals("admin")){
                request.getSession().setAttribute("User",currentUser);
                response.sendRedirect("adminPannel.html");
            }
            else if (currentUser.getType().equals("user")){
                request.getSession().setAttribute("User",currentUser);
                response.sendRedirect("userPannel.html");
            }
        }
        else{
            response.sendRedirect("login");
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Wrong username or password');");
            out.println("location='login.html';");
            out.println("</script>");
        }

        out.close();
    }
}
package servlets;

import controller.AdminController;
import model.Flight;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.List;


@WebServlet("/admin")
public class AdminServlet extends HttpServlet {
    private AdminController adminController=new AdminController();
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        List<Flight> flights=adminController.getAllFlights();
        response.setContentType("text/html");
        User currentUser= (User) request.getSession().getAttribute("User");
        if (currentUser.getType().equals("user")) response.sendRedirect("login"); ;

        PrintWriter out = response.getWriter();

        System.out.println(flights.get(0).toString());
        if (request.getParameter("flights")!=null) {
            out.print("<table>");
            out.print("<form method=" + "post" + ">");
            out.print("<table style=" + "float:left;" + ">");
            out.print("<tr>");
            out.print("<th>" + "Flight number" + "</th>");
            out.print("<th>" + "Airplane type" + "</th>");
            out.print("<th>" + "Departure city " + "</th>");
            out.print("<th>" + "Departure date " + "</th>");
            out.print("<th>" + "Arrival city" + "</th>");
            out.print("<th>" + "Arrival date" + "</th>");
            out.print("</tr>");
            for (Flight flight : flights) {
                out.print("<tr>");
                out.print("<td>" + flight.getFlightnumber() + "</td>");
                out.print("<td>" + flight.getAirplanetype() + "</td>");
                out.print("<td>" + adminController.getCityById(flight.getDepcity()).getName() + "</td>");
                out.print("<td>" + flight.getDepdate() + "</td>");
                out.print("<td>" + adminController.getCityById(flight.getArvcity()).getName() + "</td>");
                out.print("<td>" + flight.getArvdate() + "</td>");
                out.print("</tr>");
            }
            out.print("</table>");
        }else if ((request.getParameter("dflights")!=null)){
            if(request.getParameter("delFlight").equals("")){
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Please enter flight number');");
                out.println("location='adminPannel.html';");
                out.println("</script>");
            }
            else {
                if (adminController.getFlightByNumber(Integer.parseInt(request.getParameter("delFlight"))) != null) {
                    AdminController.deleteFlight(adminController.getFlightByNumber(Integer.parseInt(request.getParameter("delFlight"))));
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Flight deleted');");
                    out.println("location='adminPannel.html';");
                    out.println("</script>");
                } else {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Flight inexistent');");
                    out.println("location='adminPannel.html';");
                    out.println("</script>");
                }
            }

        }

        //request.getRequestDispatcher("admin").forward(request, response);
    }
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

       PrintWriter out = response.getWriter();

        if (request.getParameter("addflight")!=null) {
            Flight newFlight=new Flight();
            newFlight.setFlightnumber(Integer.parseInt(request.getParameter("flightnr")));
            newFlight.setAirplanetype(request.getParameter("planetype"));
            newFlight.setDepcity(adminController.getCityIdByName(request.getParameter("depcity")));
            newFlight.setDepdate(Timestamp.valueOf(request.getParameter("depdate")));
            newFlight.setArvcity(adminController.getCityIdByName(request.getParameter("arvcity")));
            newFlight.setArvdate(Timestamp.valueOf(request.getParameter("arvdate")));
            //System.out.print(newFlight.toString()+"++++++++++++++++++++++++++++++++++++++");
            adminController.createFlight(newFlight);
            //response.sendRedirect("adminPannel.html");
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Flight added');");
            out.println("location='adminPannel.html';");
            out.println("</script>");
        } else if (request.getParameter("updateflight")!=null) {
            Flight updatedFlight=new Flight();
            updatedFlight.setFlightnumber(Integer.parseInt(request.getParameter("flightnr")));
            updatedFlight.setAirplanetype(request.getParameter("planetype"));
            updatedFlight.setDepcity(adminController.getCityIdByName(request.getParameter("depcity")));
            updatedFlight.setDepdate(Timestamp.valueOf(request.getParameter("depdate")));
            updatedFlight.setArvcity(adminController.getCityIdByName(request.getParameter("arvcity")));
            updatedFlight.setArvdate(Timestamp.valueOf(request.getParameter("arvdate")));
            //System.out.print(newFlight.toString()+"++++++++++++++++++++++++++++++++++++++");
            adminController.updateFlight(updatedFlight);
            //response.sendRedirect("adminPannel.html");
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Flight updated');");
            out.println("location='adminPannel.html';");
            out.println("</script>");
        }


        out.close();

    }


}

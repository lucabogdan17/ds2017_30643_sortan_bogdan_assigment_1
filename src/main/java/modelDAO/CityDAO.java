package modelDAO;

import model.City;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class CityDAO {
    public static City getCityById(int id) {
        try {
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.openSession();

            session.beginTransaction();
            Query query = session.createQuery("from City where idcity=:id");
            query.setParameter("id", id);
            List list = query.getResultList();
            session.getTransaction().commit();

            if (list != null) return (City) list.get(0);
            else return null;
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return null;
        }
    }
    public static int getCityIdByName(String name){
        try {
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.openSession();

            session.beginTransaction();
            Query query = session.createQuery("from City where name=:name");
            query.setParameter("name", name);
            List list = query.getResultList();
            session.getTransaction().commit();

            if (list != null){
                City currentCity=(City) list.get(0);
                return currentCity.getIdcity();
            }
            else return 0;
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return 0;
        }
    }
}

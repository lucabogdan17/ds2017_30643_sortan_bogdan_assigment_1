package modelDAO;

import model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class LoginDAO {

    public User validate(User u) {
        try {
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.openSession();

            session.beginTransaction();
            Query query = session.createQuery("from User where username=:username and password=:password ");
            query.setParameter("username", u.getUsername());
            query.setParameter("password", u.getPassword());
            List list = query.getResultList();
            session.getTransaction().commit();

            if (list != null) return (User) list.get(0);
            else return null;
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return null;
        }
    }
}
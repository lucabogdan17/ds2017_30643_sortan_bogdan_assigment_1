package modelDAO;

import model.Flight;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class FlightDAO {
    public static Flight createFlight(Flight flight) {
        try {
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.openSession();
            session.beginTransaction();

            session.save(flight);

            session.getTransaction().commit();

            return flight;
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return null;
        }
    }

    public static Flight updateFlight(Flight flight) {
        try {
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.openSession();
            session.beginTransaction();

            session.merge(flight);

            session.getTransaction().commit();

            return flight;
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return null;
        }
    }

    public static Flight deleteFlight(Flight flight) {
        try {
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.openSession();
            session.beginTransaction();

            session.delete(flight);

            session.getTransaction().commit();

            return flight;
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return null;
        }
    }

    public static List<Flight> getAllFlights() {
        try {
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.openSession();
            session.beginTransaction();

            Query query = session.createQuery("from Flight ");

            List list = query.getResultList();
            session.getTransaction().commit();

            if (list != null)
            {
                System.out.println(list.toString());
                return list;
            }

            else return null;
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return null;
        }
    }

    public static Flight getFlightByNumber(int flightNumber) {
        try {
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.openSession();

            session.beginTransaction();
            Query query = session.createQuery("from Flight where flightnumber=:flightNumber");
            query.setParameter("flightNumber", flightNumber);
            List list = query.getResultList();
            session.getTransaction().commit();

            if (list != null) return (Flight) list.get(0);
            else return null;
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return null;
        }
    }

}

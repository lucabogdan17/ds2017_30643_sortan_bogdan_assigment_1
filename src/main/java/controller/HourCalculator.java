package controller;

import model.City;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.*;

public class HourCalculator {

    public static String calculateHour(City city) {
        String url = "http://new.earthtools.org/timezone/" + city.getLatitude() + "/" + city.getLongitude();

        System.out.println(url);

        InputStream is = null;
        DataInputStream dis;
        String s;
        try {

            URL connectionLink = new URL(url);

            URLConnection connection = connectionLink.openConnection();

            Document doc = parseXML(connection.getInputStream());
            NodeList descNodes = doc.getElementsByTagName("localtime");

            for(int i=0; i<descNodes.getLength();i++)
            {
                System.out.println(descNodes.item(i).getTextContent());
                return(descNodes.item(i).getTextContent());
            }

        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return null;
    }

    private static Document parseXML(InputStream stream)
            throws Exception
    {
        DocumentBuilderFactory objDocumentBuilderFactory = null;
        DocumentBuilder objDocumentBuilder = null;
        Document doc = null;
        try
        {
            objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
            objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

            doc = objDocumentBuilder.parse(stream);
        }
        catch(Exception ex)
        {
            throw ex;
        }

        return doc;
    }
    }

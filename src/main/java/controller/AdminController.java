package controller;

import model.City;
import model.Flight;
import modelDAO.CityDAO;
import modelDAO.FlightDAO;

import java.util.List;

public class AdminController {
    static FlightDAO flightDAO=new FlightDAO();
    static CityDAO cityDAO=new CityDAO();

    public static Flight createFlight(Flight flight) {
        return flightDAO.createFlight(flight);
    }

    public static Flight updateFlight(Flight flight) {
        Flight currentFlight=flightDAO.getFlightByNumber(flight.getFlightnumber());
        flight.setIdflights(currentFlight.getIdflights());
        if (flight.getAirplanetype().equals(null)) flight.setAirplanetype(currentFlight.getAirplanetype());
        if (flight.getDepcity().equals(null)) flight.setDepcity(currentFlight.getDepcity());
        if (flight.getArvcity().equals(null)) flight.setArvcity(currentFlight.getArvcity());
        if (flight.getDepdate().equals(null)) flight.setDepdate(currentFlight.getDepdate());
        if (flight.getArvdate().equals(null)) flight.setArvdate(currentFlight.getArvdate());

        return flightDAO.updateFlight(flight);
    }

    public static Flight deleteFlight(Flight flight) {
       return flightDAO.deleteFlight(flight);
    }

    public static List<Flight> getAllFlights() {
       return flightDAO.getAllFlights();
    }

    public Flight getFlightByNumber(int flightNumber) {
        return flightDAO.getFlightByNumber(flightNumber);
    }

    public City getCityById(int id){
        return cityDAO.getCityById(id);
    }

    public int getCityIdByName(String cityName){
        return cityDAO.getCityIdByName(cityName);
    }


}

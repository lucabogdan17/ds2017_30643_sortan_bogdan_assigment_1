package controller;

import model.City;
import model.Flight;
import modelDAO.CityDAO;
import modelDAO.FlightDAO;

import java.util.List;

public class UserController {
    static FlightDAO flightDAO=new FlightDAO();
    static CityDAO cityDAO=new CityDAO();

    public static List<Flight> getAllFlights() {
        return flightDAO.getAllFlights();
    }


    public City getCityById(int id){
        return cityDAO.getCityById(id);
    }

    public int getCityIdByName(String cityName){
        return cityDAO.getCityIdByName(cityName);
    }

    public Flight getFlightByNumber(int flightNumber) {
        return flightDAO.getFlightByNumber(flightNumber);
    }

}

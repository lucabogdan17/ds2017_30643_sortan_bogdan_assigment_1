package model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Flight {
    private int idflights;
    private Integer flightnumber;
    private String airplanetype;
    private Integer depcity;
    private Timestamp depDate;
    private Integer arvcity;
    private Timestamp arvdate;

    @Id
    @Column(name = "idflights")
    public int getIdflights() {
        return idflights;
    }

    public void setIdflights(int idflights) {
        this.idflights = idflights;
    }

    @Basic
    @Column(name = "flightnumber")
    public Integer getFlightnumber() {
        return flightnumber;
    }

    public void setFlightnumber(Integer flightnumber) {
        this.flightnumber = flightnumber;
    }

    @Basic
    @Column(name = "airplanetype")
    public String getAirplanetype() {
        return airplanetype;
    }

    public void setAirplanetype(String airplanetype) {
        this.airplanetype = airplanetype;
    }

    @Basic
    @Column(name = "depcity")
    public Integer getDepcity() {
        return depcity;
    }

    public void setDepcity(Integer depcity) {
        this.depcity = depcity;
    }

    @Basic
    @Column(name = "depdate")
    public Timestamp getDepdate() {
        return depDate;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "idflights=" + idflights +
                ", flightnumber=" + flightnumber +
                ", airplanetype='" + airplanetype + '\'' +
                ", depcity=" + depcity +
                ", depDate=" + depDate +
                ", arvcity=" + arvcity +
                ", arvdate=" + arvdate +
                '}';
    }

    public void setDepdate(Timestamp depDate) {
        this.depDate = depDate;
    }

    @Basic
    @Column(name = "arvcity")
    public Integer getArvcity() {
        return arvcity;
    }

    public void setArvcity(Integer arvcity) {
        this.arvcity = arvcity;
    }

    @Basic
    @Column(name = "arvdate")
    public Timestamp getArvdate() {
        return arvdate;
    }

    public void setArvdate(Timestamp arvdate) {
        this.arvdate = arvdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flight flight = (Flight) o;

        if (idflights != flight.idflights) return false;
        if (flightnumber != null ? !flightnumber.equals(flight.flightnumber) : flight.flightnumber != null)
            return false;
        if (airplanetype != null ? !airplanetype.equals(flight.airplanetype) : flight.airplanetype != null)
            return false;
        if (depcity != null ? !depcity.equals(flight.depcity) : flight.depcity != null) return false;
        if (depDate != null ? !depDate.equals(flight.depDate) : flight.depDate != null) return false;
        if (arvcity != null ? !arvcity.equals(flight.arvcity) : flight.arvcity != null) return false;
        if (arvdate != null ? !arvdate.equals(flight.arvdate) : flight.arvdate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idflights;
        result = 31 * result + (flightnumber != null ? flightnumber.hashCode() : 0);
        result = 31 * result + (airplanetype != null ? airplanetype.hashCode() : 0);
        result = 31 * result + (depcity != null ? depcity.hashCode() : 0);
        result = 31 * result + (depDate != null ? depDate.hashCode() : 0);
        result = 31 * result + (arvcity != null ? arvcity.hashCode() : 0);
        result = 31 * result + (arvdate != null ? arvdate.hashCode() : 0);
        return result;
    }
}
